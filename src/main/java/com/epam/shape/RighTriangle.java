package com.epam.shape;

public class RighTriangle extends Triangle {
    public RighTriangle(double a, double b) {
        super(a, b);
    }

    public double squere() {
        return a*b/2;
    }

    public double perimetr() {
        return a+b+gipitenusa();
    }
    public double gipitenusa(){
        double G = Math.sqrt(a*a+b*b);
        return G;
    }
}
