package com.epam.shape;

public abstract class Shape {
    public abstract double squere();
    public abstract double perimetr();
}
