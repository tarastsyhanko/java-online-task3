package com.epam.shape;

public class IsoscelesTriangle extends Triangle {
    public IsoscelesTriangle(double a, double b){
        super(a, b);
    }

    public double squere() {
        return a*b/2;
    }

    public double perimetr() {
        return a+a+b;
    }
}
