package com.epam.shape;

public  class Circle extends Shape {
    private double radius;
    public Circle(double radius){
        this.radius = radius;
    }

    public double squere() {
        return Math.PI*radius*radius;
    }

    public double perimetr() {
        return 2*Math.PI*radius;
    }
}
