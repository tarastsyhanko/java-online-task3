package com.epam.shape;

public abstract class Triangle extends Shape {
    protected double a;
    protected double b;

    public Triangle(double a, double b) {
        this.a = a;
        this.b = b;
    }
    public abstract double squere();
    public abstract double perimetr();
}
