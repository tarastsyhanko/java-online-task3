package com.epam.enimals;

public abstract class Animal {
    protected abstract void sleep();

    protected abstract void eat(Object eateverything);
}
