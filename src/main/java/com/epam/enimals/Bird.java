package com.epam.enimals;

public abstract class Bird extends Animal {
    protected abstract void fly();

    protected abstract void sleep();

    protected abstract void eat(Object eatevrything);
}

