package com.epam.enimals;

public abstract class Fish extends Animal {
    protected abstract void swim();
    protected abstract void sleep();
    protected abstract void eat(Object eatevrything);
}
